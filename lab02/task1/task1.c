#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define a 1140671485
#define b 12820163
#define m (2 << 24)
#define DIAPASON 250
#define LENGTH 20000

int pseudo_random() {
    static int x = 0;
    x = (a * x + b) % m;
    return x % DIAPASON;
}

int main() {
    int frequency[DIAPASON] = { 0 };
    double probability[DIAPASON] = { 0.0 };

    int numbers[LENGTH];

    for (int i = 0; i < LENGTH; i++) {
        numbers[i] = pseudo_random();
        frequency[numbers[i]]++;
    }

    double mean = 0.0;
    double variance = 0.0;

    for (int i = 0; i < DIAPASON; i++) {
        probability[i] = (double)frequency[i] / LENGTH;
        mean += i * probability[i];
    }

    for (int i = 0; i < DIAPASON; i++) {
        variance += (i - mean) * (i - mean) * probability[i];
    }

    double standard_deviation = sqrt(variance);

    printf("Frequency of appearance of random numbers:\n");
    for (int i = 0; i < DIAPASON; i++) {
        printf("%d: %d |\t", i + 1, frequency[i]);
    }
    
    printf("\n");

    printf("\nStatistical probability of appearance of random numbers:\n");
    for (int i = 0; i < DIAPASON; i++) {
        printf("%d: %.6f |\t", i + 1, probability[i]);
    }

    printf("\n");

    printf("\nMean: %.6f\n", mean);
    printf("Variance: %.6f\n", variance);
    printf("Standard deviation: %.6f\n", standard_deviation);

    return 0;
}