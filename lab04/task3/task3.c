#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_SIZE 100

// ???? ??? ?????????? ????????? ?? ????????? ???????????
typedef struct {
    double data[MAX_SIZE];
    int top;
} Stack;

void initialize(Stack* s) {
    s->top = -1;
}

void push(Stack* s, double value) {
    if (s->top == MAX_SIZE - 1) {
        printf("Stack overflow.\n");
        exit(1);
    }
    s->data[++s->top] = value;
}

double pop(Stack* s) {
    if (s->top == -1) {
        printf("Stack underflow.\n");
        exit(1);
    }
    return s->data[s->top--];
}

// ???????, ??? ???????? ???????? ??????, ?????????????? ???????? ???????? ???????
double evaluateExpression(char* expr) {
    Stack operands;
    initialize(&operands);

    char* token = NULL;
    char* nextToken = NULL;
    token = strtok_s(expr, " ", &nextToken);
    while (token != NULL) {
        if (isdigit(token[0])) {
            push(&operands, atof(token));
        }
        else {
            double operand2 = pop(&operands);
            double operand1 = pop(&operands);

            double result;
            switch (token[0]) {
            case '+':
                result = operand1 + operand2;
                break;
            case '-':
                result = operand1 - operand2;
                break;
            case '*':
                result = operand1 * operand2;
                break;
            case '/':
                result = operand1 / operand2;
                break;
            case '^':
                result = pow(operand1, operand2);
                break;
            case 's':
                result = sqrt(operand2);
                break;
            default:
                printf("Invalid operator: %s\n", token);
                exit(1);
            }
            push(&operands, result);
        }
        token = strtok_s(NULL, " ", &nextToken);
    }
    return pop(&operands);
}

int main() {
    char expr[MAX_SIZE * 10];

    printf("Enter expression in reverse Polish notation (operands separated by spaces): ");
    fgets(expr, sizeof(expr), stdin);
    expr[strcspn(expr, "\n")] = '\0';

    double result = evaluateExpression(expr);
    printf("Result: %lf\n", result);

    return 0;
}
