#include <stdio.h>
#include <stdlib.h>

// ????????? ????? ?????'?????? ??????
typedef struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
} Node;

// ??????? ??? ????????? ?????? ?????
Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

// ??????? ??? ????????? ?????? ????? ? ?????? ??????
void append(Node** head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
    newNode->prev = temp;
}

// ??????? ??? ????????? ????? ?? ??????
void deleteNode(Node** head, int key) {
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }
    Node* temp = *head;
    if (temp->data == key) {
        *head = temp->next;
        free(temp);
        return;
    }
    while (temp != NULL && temp->data != key) {
        temp = temp->next;
    }
    if (temp == NULL) {
        printf("Element not found in the list.\n");
        return;
    }
    temp->prev->next = temp->next;
    if (temp->next != NULL) {
        temp->next->prev = temp->prev;
    }
    free(temp);
}

// ??????? ??? ????????? ?????? ??????
void display(Node* head) {
    if (head == NULL) {
        printf("List is empty.\n");
        return;
    }
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

// ??????? ??? ???????? ??????
void destroyList(Node** head) {
    Node* current = *head;
    Node* next;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
    *head = NULL;
}

int main() {
    Node* head = NULL;
    int choice, data;

    do {
        printf("\n1. Add element\n");
        printf("2. Delete element\n");
        printf("3. Display list\n");
        printf("4. Destroy list\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf_s("%d", &choice);

        switch (choice) {
        case 1:
            printf("Enter data to add: ");
            scanf_s("%d", &data);
            append(&head, data);
            break;
        case 2:
            printf("Enter data to delete: ");
            scanf_s("%d", &data);
            deleteNode(&head, data);
            break;
        case 3:
            printf("List: ");
            display(head);
            break;
        case 4:
            destroyList(&head);
            printf("List destroyed.\n");
            break;
        case 5:
            printf("Exiting...\n");
            break;
        default:
            printf("Invalid choice!\n");
        }
    } while (choice != 5);

    return 0;
}
