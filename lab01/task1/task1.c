#include <stdio.h>
#include <time.h>

struct CompactDateTime {
    unsigned int year;      // Year
    unsigned char month;    // Month (1-12)
    unsigned char day;      // Day (1-31)
    unsigned char hour;     // Hour (0-23)
    unsigned char minute;   // Minute (0-59)
    unsigned char second;   // Second (0-59)
};

int main() {
    time_t now;
    time(&now);
    struct tm* tm_now = localtime(&now);

    struct CompactDateTime compactDateTime;
    compactDateTime.year = tm_now->tm_year + 1900;
    compactDateTime.month = tm_now->tm_mon + 1;
    compactDateTime.day = tm_now->tm_mday;
    compactDateTime.hour = tm_now->tm_hour;
    compactDateTime.minute = tm_now->tm_min;
    compactDateTime.second = tm_now->tm_sec;

    printf("Size of CompactDateTime structure: %lu bytes\n", sizeof(compactDateTime));

    printf("CompactDateTime: %d-%02d-%02d %02d:%02d:%02d\n",
        compactDateTime.year, compactDateTime.month, compactDateTime.day,
        compactDateTime.hour, compactDateTime.minute, compactDateTime.second);

    printf("Size of time_t: %lu bytes\n", sizeof(time_t));

    return 0;
}
