#include <stdio.h>

int main() {
    // a) 5 + 127
    signed char a = 5;
    signed char b = 127;
    signed char result_a = a + b;
    printf("Result a): %d\n", result_a);

    // b) 2 - 3
    signed char c = 2;
    signed char d = -3;
    signed char result_b = c - d;
    printf("Result b): %d\n", result_b);

    // c) -120 - 34
    signed char e = -120;
    signed char f = -34;
    signed char result_c = e - f;
    printf("Result c): %d\n", result_c);

    // d) (unsigned char)(-5)
    signed char g = -5;
    unsigned char result_d = (unsigned char)g;
    printf("Result d): %u\n", result_d);

    // e) 56 & 38
    signed char h = 56;
    signed char i = 38;
    signed char result_e = h & i;
    printf("Result e): %d\n", result_e);

    // f) 56 | 38
    signed char j = 56;
    signed char k = 38;
    signed char result_f = j | k;
    printf("Result f): %d\n", result_f);

    return 0;
}