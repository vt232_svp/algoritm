#include <stdio.h>

union CompactFloat {
    float floatValue;
    unsigned char bytes[sizeof(float)];
};

int main() {
    float input;
    printf("Enter a float number: ");
    scanf_s("%f", &input);

    union CompactFloat number;
    number.floatValue = input;

    printf("Value byte by byte: ");
    for (int i = 0; i < sizeof(float); i++) {
        printf("%02X ", number.bytes[i]);
    }
    printf("\n");

    printf("Value bit by bit: ");
    for (int i = sizeof(float) - 1; i >= 0; i--) {
        for (int j = 7; j >= 0; j--) {
            printf("%d", (number.bytes[i] >> j) & 1);
        }
        printf(" ");
    }
    printf("\n");

    int sign = (number.bytes[sizeof(float) - 1] >> 7) & 1;
    if (sign == 0) {
        printf("Sign: positive\n");
    }
    else {
        printf("Sign: negative\n");
    }
    unsigned int mantissa = ((number.bytes[sizeof(float) - 2] & 0x7F) << 16) | (number.bytes[sizeof(float) - 3] << 8) | number.bytes[sizeof(float) - 4];
    printf("Mantissa: %u\n", mantissa);

    printf("Memory size: %lu bytes\n", sizeof(union CompactFloat));

    return 0;
}
