﻿#include <stdio.h>

union IntByte {
    signed short intValue;  // ціле число
    unsigned char bytes[2];  // масив байтів
};

int main() {
    signed short input;
    printf("Enter a signed short integer value: ");
    scanf_s("%hd", &input);

    union IntByte number;
    number.intValue = input;

    printf("Entered number: %d\n", number.intValue);
    if (number.bytes[1] & 0x80) {
        printf("Sign: negative\n");
        printf("Value (in absolute): %d\n", -number.intValue);
    }
    else {
        printf("Sign: positive\n");
        printf("Value: %d\n", number.intValue);
    }

    return 0;
}
