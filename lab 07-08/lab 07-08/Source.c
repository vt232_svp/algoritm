#include <stdio.h>
#include <stdlib.h>

#define MAX 20

int adjMatrix[MAX][MAX];
int visited[MAX];

void createGraph();
void displayGraph();
void DFS(int vertex);
void BFS(int startVertex);

int main() {
    createGraph();

    displayGraph();

    printf("\nDFS Traversal starting from Kyiv (0):\n");
    DFS(0);

    for (int i = 0; i < MAX; i++) {
        visited[i] = 0;
    }

    printf("\n\nBFS Traversal starting from Kyiv (0):\n");
    BFS(0);

    return 0;
}

void createGraph() {
    for (int i = 0; i < MAX; i++) {
        for (int j = 0; j < MAX; j++) {
            adjMatrix[i][j] = 0;
        }
    }

    adjMatrix[0][1] = 135; adjMatrix[1][2] = 80; adjMatrix[2][3] = 100; adjMatrix[3][4] = 68;
    adjMatrix[0][1] = 135; adjMatrix[1][5] = 38; adjMatrix[5][6] = 73; adjMatrix[6][7] = 110; adjMatrix[7][8] = 104;
    adjMatrix[0][1] = 135; adjMatrix[1][9] = 115;
    adjMatrix[0][10] = 78; adjMatrix[10][11] = 115;
    adjMatrix[0][10] = 78; adjMatrix[10][12] = 146; adjMatrix[12][13] = 105;
    adjMatrix[0][10] = 78; adjMatrix[10][14] = 181; adjMatrix[14][15] = 130;
    adjMatrix[0][16] = 128; adjMatrix[16][17] = 175;
    adjMatrix[0][16] = 128; adjMatrix[16][18] = 109;

    for (int i = 0; i < MAX; i++) {
        for (int j = 0; j < MAX; j++) {
            if (adjMatrix[i][j] != 0) {
                adjMatrix[j][i] = adjMatrix[i][j];
            }
        }
    }
}

void displayGraph() {
    printf("Adjacency Matrix:\n");
    for (int i = 0; i < MAX; i++) {
        for (int j = 0; j < MAX; j++) {
            printf("%4d ", adjMatrix[i][j]);
        }
        printf("\n");
    }
}

void DFS(int vertex) {
    printf("%d ", vertex);
    visited[vertex] = 1;
    for (int i = 0; i < MAX; i++) {
        if (adjMatrix[vertex][i] != 0 && !visited[i]) {
            DFS(i);
        }
    }
}

void BFS(int startVertex) {
    int queue[MAX], front = 0, rear = -1;
    printf("%d ", startVertex);
    visited[startVertex] = 1;
    queue[++rear] = startVertex;

    while (front <= rear) {
        int currentVertex = queue[front++];
        for (int i = 0; i < MAX; i++) {
            if (adjMatrix[currentVertex][i] != 0 && !visited[i]) {
                printf("%d ", i);
                visited[i] = 1;
                queue[++rear] = i;
            }
        }
    }
}
