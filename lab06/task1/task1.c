#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void heapify(double arr[], int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && arr[left] > arr[largest])
        largest = left;

    if (right < n && arr[right] > arr[largest])
        largest = right;

    if (largest != i) {
        double temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;
        heapify(arr, n, largest);
    }
}

void heapSort(double arr[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i >= 0; i--) {
        double temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        heapify(arr, i, 0);
    }
}

void shellSort(double arr[], int n) {
    for (int gap = n / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < n; i++) {
            double temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

void countingSortShort(short arr[], int n, short min, short max) {
    int range = max - min + 1;
    int* count = (int*)malloc(range * sizeof(int));
    short* output = (short*)malloc(n * sizeof(short));

    if (count == NULL || output == NULL) {
        printf("Memory allocation failed for count or output arrays.\n");
        return;
    }

    for (int i = 0; i < range; i++)
        count[i] = 0;

    for (int i = 0; i < n; i++)
        count[arr[i] - min]++;

    for (int i = 1; i < range; i++)
        count[i] += count[i - 1];

    for (int i = n - 1; i >= 0; i--) {
        output[count[arr[i] - min] - 1] = arr[i];
        count[arr[i] - min]--;
    }

    for (int i = 0; i < n; i++)
        arr[i] = output[i];

    free(count);
    free(output);
}

void generateRandomNumbers(double arr[], int size, double lower, double upper) {
    for (int i = 0; i < size; i++)
        arr[i] = lower + (double)rand() / RAND_MAX * (upper - lower);
}

void generateRandomNumbersShort(short arr[], int size, short lower, short upper) {
    for (int i = 0; i < size; i++)
        arr[i] = lower + rand() % (upper - lower + 1);
}

double measureTime(void (*sortingFunction)(void*, int), void* data, int size) {
    clock_t start, end;
    start = clock();
    sortingFunction(data, size);
    end = clock();
    return ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
}

double measureTimeShort(void (*sortingFunction)(void*, int, short, short), void* data, int size, short min, short max) {
    clock_t start, end;
    start = clock();
    sortingFunction(data, size, min, max);
    end = clock();
    return ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
}

void measureAndWriteTimes(FILE* fp, void (*sortingFunction)(void*, int), void* data, int size) {
    double time = measureTime(sortingFunction, data, size);
    fprintf(fp, "%.6f,", time);
}

void measureAndWriteTimesShort(FILE* fp, void (*sortingFunction)(void*, int, short, short), void* data, int size, short min, short max) {
    double time = measureTimeShort(sortingFunction, data, size, min, max);
    fprintf(fp, "%.6f,", time);
}

int main() {
    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    int numSizes = sizeof(sizes) / sizeof(sizes[0]);

    FILE* fp = fopen("times.csv", "w");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    fprintf(fp, "Size,Heap_Sort,Shell_Sort,Counting_Sort\n");

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i];

        double* heapArr = (double*)malloc(size * sizeof(double));
        if (heapArr == NULL) {
            printf("Memory allocation failed for heapArr.\n");
            return 1;
        }
        generateRandomNumbers(heapArr, size, 0.0, 400.0);

        double* shellArr = (double*)malloc(size * sizeof(double));
        if (shellArr == NULL) {
            printf("Memory allocation failed for shellArr.\n");
            free(heapArr);
            return 1;
        }
        generateRandomNumbers(shellArr, size, -100.0, 10.0);

        short* countingArr = (short*)malloc(size * sizeof(short));
        if (countingArr == NULL) {
            printf("Memory allocation failed for countingArr.\n");
            free(heapArr);
            free(shellArr);
            return 1;
        }
        generateRandomNumbersShort(countingArr, size, -25, 10);

        fprintf(fp, "%d,", size);
        measureAndWriteTimes(fp, heapSort, heapArr, size);
        measureAndWriteTimes(fp, shellSort, shellArr, size);
        measureAndWriteTimesShort(fp, countingSortShort, countingArr, size, -25, 10);
        fprintf(fp, "\n");

        free(heapArr);
        free(shellArr);
        free(countingArr);
    }

    fclose(fp);

    return 0;
}
