#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// ????????? ???????? ??????????? ??????
typedef struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
} Node;

// ????????? ??????????? ??????
typedef struct List {
    Node* head;
} List;

// ??????? ??? ????????????? ??????
List* initializeList() {
    List* list = (List*)malloc(sizeof(List));
    list->head = NULL;
    return list;
}

// ??????? ??? ????????? ???????? ? ?????? ??????
void append(List* list, int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;

    if (list->head == NULL) {
        list->head = newNode;
    }
    else {
        Node* current = list->head;
        while (current->next != NULL)
            current = current->next;
        current->next = newNode;
        newNode->prev = current;
    }
}

// ??????? ??? ????????? ?????? ????????? ?????
void generateRandomNumbers(int arr[], int size) {
    srand(time(NULL));
    for (int i = 0; i < size; i++)
        arr[i] = rand() % 1000;
}

// ?????????? ??????? (?????????? ??????)
void selectionSort(List* list) {
    Node* current = list->head;
    while (current != NULL) {
        Node* min = current;
        Node* temp = current->next;
        while (temp != NULL) {
            if (temp->data < min->data)
                min = temp;
            temp = temp->next;
        }
        int tempData = current->data;
        current->data = min->data;
        min->data = tempData;
        current = current->next;
    }
}

// ??????? ??? ??????? ???????? ? ????????????? ??????
void sortedInsert(Node** head_ref, Node* new_node) {
    Node* current;
    if (*head_ref == NULL || (*head_ref)->data >= new_node->data) {
        new_node->next = *head_ref;
        new_node->prev = NULL;
        if (*head_ref != NULL)
            (*head_ref)->prev = new_node;
        *head_ref = new_node;
    }
    else {
        current = *head_ref;
        while (current->next != NULL && current->next->data < new_node->data)
            current = current->next;
        new_node->next = current->next;
        if (current->next != NULL)
            current->next->prev = new_node;
        current->next = new_node;
        new_node->prev = current;
    }
}

// ?????????? ????????? (?????????? ??????)
void insertionSortList(List* list) {
    Node* sorted = NULL;
    Node* current = list->head;
    while (current != NULL) {
        Node* next = current->next;
        sortedInsert(&sorted, current);
        current = next;
    }
    list->head = sorted;
}

// ?????????? ????????? (?????)
void insertionSortArray(int arr[], int size) {
    int i, key, j;
    for (i = 1; i < size; i++) {
        key = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

// ??????? ??? ??????????? ???? ????????? ??????????
double measureTime(void (*sortingFunction)(void*, int), void* data, int size) {
    clock_t start, end;
    start = clock();
    sortingFunction(data, size);
    end = clock();
    return ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
}

// ??????? ??? ??????????? ?? ?????? ???? ?????????? ? ????
void measureAndWriteTimes(FILE* fp, void (*sortingFunction)(void*, int), void* data, int size) {
    double time = measureTime(sortingFunction, data, size);
    fprintf(fp, "%.6f ", time);
}

int main() {
    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    int numSizes = sizeof(sizes) / sizeof(sizes[0]);

    FILE* fp = fopen("times.csv", "w");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    fprintf(fp, "Size,Selection_Sort,Insertion_Sort_List,Insertion_Sort_Array\n");

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i];
        int* arr = (int*)malloc(size * sizeof(int));
        generateRandomNumbers(arr, size);

        List* list = initializeList();
        for (int j = 0; j < size; j++)
            append(list, arr[j]);

        fprintf(fp, "%d,", size);
        measureAndWriteTimes(fp, selectionSort, list, size);
        fprintf(fp, ",");
        measureAndWriteTimes(fp, insertionSortList, list, size);
        fprintf(fp, ",");
        measureAndWriteTimes(fp, insertionSortArray, arr, size);
        fprintf(fp, "\n");

        free(arr);
        free(list);
    }

    fclose(fp);

    return 0;
}
