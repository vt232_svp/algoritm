#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

// task 1
long long maxPossibleNumber(int digits[], int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (digits[j] < digits[j + 1]) {
                int temp = digits[j];
                digits[j] = digits[j + 1];
                digits[j + 1] = temp;
            }
        }
    }

    long long number = 0;
    for (int i = 0; i < n; i++) {
        number = number * 10 + digits[i];
    }

    return number;
}

// task 2
char* binaryRepresentation(int a, int b) {
    char* binary = (char*)malloc((b + 1) * sizeof(char));
    if (binary == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    for (int i = b - 1; i >= 0; i--) {
        binary[b - 1 - i] = ((a & (1 << i)) ? '1' : '0');
    }
    binary[b] = '\0';

    return binary;
}

int main() {
    int digits[] = { 3, 5, 7, 2, 9 };
    int n = sizeof(digits) / sizeof(digits[0]);

    clock_t start1 = clock();
    long long maxNumber = maxPossibleNumber(digits, n);
    clock_t end1 = clock();
    double time_taken1 = ((double)(end1 - start1)) / (CLOCKS_PER_SEC / 1000);
    printf("Max possible number: %lld\n", maxNumber);
    printf("Time taken for task 1: %f milliseconds\n", time_taken1);

    srand(time(NULL));
    int randomNum = rand() % 1000;
    int b = 8 + rand() % 57;

    clock_t start2 = clock();
    char* binaryRep = binaryRepresentation(randomNum, b);
    clock_t end2 = clock();
    double time_taken2 = ((double)(end2 - start2)) / (CLOCKS_PER_SEC / 1000);

    printf("Binary representation of %d: %s\n", randomNum, binaryRep);
    printf("Time taken for task 2: %f milliseconds\n", time_taken2);

    FILE* fp = fopen("time_taken.csv", "w");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }
    fprintf(fp, "Task,Time\n");
    fprintf(fp, "Task 1,%f\n", time_taken1);
    fprintf(fp, "Task 2,%f\n", time_taken2);
    fclose(fp);

    free(binaryRep);

    return 0;
}
