﻿#include <stdio.h>
#include <math.h>

#define MAX_N 50
#define MAX_Y 500

double f1(int n) { return n; }
double f2(int n) { return log(n); }
double f3(int n) { return n * log(n); }
double f4(int n) { return n * n; }
double f5(int n) { return pow(2, n); }

long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

int main() {
    FILE* fp = fopen("table.csv", "w");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    fprintf(fp, "n,f1(n),f2(n),f3(n),f4(n),f5(n),f6(n)\n");

    for (int n = 0; n <= MAX_N; n++) {
        double y1 = f1(n);
        double y2 = f2(n);
        double y3 = f3(n);
        double y4 = f4(n);
        double y5 = f5(n);
        long long y6 = factorial(n);

        fprintf(fp, "%d,%.2f,%.2f,%.2f,%.2f,%.2f,%lld\n", n, y1, y2, y3, y4, y5, y6);
    }

    fclose(fp);
    return 0;
}